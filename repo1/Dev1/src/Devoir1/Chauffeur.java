package Devoir1;

public class Chauffeur {  
	private static final char[] Chauffeur = null;
	private String num,nom,prenom,adr;
	private int ann;
	private TrajetList Tra;
	
	public Chauffeur(String num, String nom, String prenom, int ann, String adr, TrajetList Tra) {
	this.num= num;
	this.nom= nom;
	this.prenom= prenom;
	this.ann= ann;
	this.adr= adr;
	this.Tra= Tra;
}
	
	public TrajetList getTra() {
		return Tra;
	}
	
	public String getNum() {
		return num;
	}
	public String toString() {
	    return nom+ " "+ prenom + "   "+ ann+ " employ�"+ "   "+ adr;
	}
	
	public void display(Chauffeur chau) throws WrongInputException {
		System.out.println(chau);
		System.out.println("\nles trajets:\n");
		getTra().display();
		
	}
	
	public void displayLim(Chauffeur chau) {
		getTra().displayLim();
	}
 
	
}
